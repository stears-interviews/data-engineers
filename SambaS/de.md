# Stears Coding Homework

Thanks for your application to Stears

This is a coding homework test for Stears' Data Engineer role. We expect that you will use this test to demonstrate your abilities in SQL, Python and data engineering. Your solution does not need to be perfect, but keep in mind we are hiring for someone to improve and maintain our data pipeline to inform our product decisions and serve our customers data needs, and your solution will reflect how well you are able to do so.

For this role, we prefer candidates with
- Excellent SQL skills
- Good knowledge of Python libraries (Pandas, Numpy, Jupyter notebook)

For each task, you will be given some context and a clear definition of done. Either complete all of them or attempt as much as possible so we have enough information to assess your skill & experience.

#### Submissions

- Create a private [gitlab](https://gitlab.com/) repository and add @foluso_ogunlana & @ssani as maintainers
- Create a README.md with the following:
  - Clear set up instructions (assume no knowledge of the submission or the stack)
  - Notes of any new useful tools / patterns you discovered while completing the test

#### Deadlines

You will be given at least 5 working days to complete the task and your deadline will be communicated via email.

#### Assessment

Our favourite submissions are -

1. **Clean & simple code** - Minimal, high quality and well tested code that gets to the heart of the problem and stops there.
2. **Rigorous** - Complete solutions with well researched applications of the right technology choices.
3. **UX & DevX** - Smooth user / developer experience which is easy to deploy, well documented and easy to contribute to.

Your submission will also impress us by demonstrating one or more of the following -

- **Mastery** in your general use of language, libraries, frameworks, architecture / design / deployment patterns
- **Unique skills** in specific areas - particularly data engineering, devops & testing

#### Conduct

By submitting, you assure us that you have not shared the test with anyone else and that all work submitted is your own. Though you are allowed to use whatever online or offline resources you see fit to learn skills for the tasks.

# **Coding Homework**

**Guidelines**
- Complete all tasks if possible
- Include a README.md telling us how to use your code
- Database - Prefer PostgreSQL

## Background

The SambaS is an app that teaches you to dance. Users can create a free account, or a paid subscription. Usually users will first enjoy a free account until they feel the need to upgrade to become subscribers. The SambaS subscriber base has been declining for months and the growth marketer, Maman, wants to figure out why by looking at the data. The SambaS analytics database has two tables that you can use to figure this out.

1. current_subscribers - A list of the current subscribers to SambaS
2. lost_subscribers - A list of people who previously subscribed but have now lost their subscriptions

### Task 1

Maman needs a SQL query to run each day, that can tell how many subscribers were added in the previous day, how many were lost in the previous day, and finally, how many were lost yesterday due to a specific reason which the SambaS team cares about (Failed rebilling).

Using the data in the csvs above ([current_subscribers.csv](/SambaS/current_subscribers.csv) and [lost_subscribers.csv](/SambaS/lost_subscribers.csv)), load a SQL database with the two tables and write a SQL query (in a file "yesterdays_subscriptions.sql") to create the following table:

| Metric                           | Count                    |
| -------------------------------- | ------------------------ |
| Added yesterday                  | 3 (1 were reactivations) |
| Lost yesterday                   | 1                        |
| Lost yesterday: Failed rebilling | 1                        |

**Definition of done**:

- The query must be valid SQL and executable on PostgreSQL 13
- The resulting table must have two columns - Metric and Count
- There must be 3 rows in the resulting table - "Added yesterday", "Lost yesterday" and "Lost yesterday: Failed rebilling"
- The count of "Added yesterday" must be a string in the format "X (Y were reactivations)" where X is the total number of subscriptions that have a "start_date" of the previous day, and Y is the number of subscriptions added yesterday with "reactivation_of_previous_sub" set to `true`
- The count of "Lost yesterday" must be an integer and the sum of all subscriptions that were lost on the previous day
- The count of "Lost yesterday: Failed rebilling" must be an integer and the sum of all subscriptions that were lost on the previous day

### Task 2

Maman is grateful because you have saved a lot of time for the team, and have allowed the team to investigate any abnormal swings in subscriber growth. Now, Maman needs to plan for successful future growth campaigns by studying past trends in the net subscriber growth numbers.

Using the data in the csvs above (current_subscribers and lost_subscribers), load a SQL database with the two tables and write a SQL query (in a file "weekly_net_subscriptions.sql") to create the following table:

| Week                     | Added | Lost | Net (Added - Lost) |
| ------------------------ | ----- | ---- | ------------------ |
| May 9, 2022, 12:00 AM    | 22    | -13  | 9                  |
| May 2, 2022, 12:00 AM    | 59    | -33  | 26                 |
| April 25, 2022, 12:00 AM | 31    | -27  | 4                  |

**Definition of done**:

- The query must be valid SQL and executable on PostgreSQL 13
- The resulting table must have four columns - Week, Added, Lost, Net, and each row must represent a single week
- In the "Week" column, each cell should be the first day of the week for every week from the earliest subscription / loss in the data, until the latest. (You can skip weeks without data)
- The "Added" column must count the subscriptions added for the week
- The "Lost" column must count the subscriptions lost for the week
- The "Net" column must count the net subscriptions added (added - lost) for the week

### Task 3

Now that the team is able to see the trends in subscriber growth, the team is trying to figure out how to get more people to subscribe. The team doesn't yet know what to do, but they have a lot of granular fact/event based questions to ask the data. Can you design and visualise a schema of tables that will allow us make queries for the data we need? Please provide mock queries for the schema.

**Hint**
- You don't have to provide any data. Just provide a diagram showing the tables and at least 4 example queries
- You can find the example questions to be answered below the definition of done.

**Definition of done**

- Database schema (tables) visualisation attached as a file ./task_3_schema.png (in whatever file extension)
- The tables must define all necessary column names and types, as well as foreign and primary keys
- At least 4 queries must be provided (at least one from each section below - Usage, Engagement, Content, Growth) that show how to fetch data from the tables

Below are some of the questions the team wishes to answer with your schema. You do not need to provide queries for all of them, but you need to provide a schema that can be used to answer all of them.

Usage
- Which and how many users / subscribers opened our app today?

Engagement
- Which and how many users / subscribers started a dance today?
- Which and how many users / subscribers completed a dance today?

Content
- Which dances were most popular today?
- Which dance styles were the least popular today?
- Which dancing instructors have the largest following?

Growth
- Which and how many users converted to subscribers today?
- Which and how many subscribers cancelled their subscriptions today?
- On average, how many dances does it take before a user converts to a paying subscriber?
- Which dances are historically most likely to convert a user to a subscriber?


### Task 4

Now that the SambaS team is able to understand the behaviour of cohorts and individual users, their final wish is to quantify the probability that a user will subscribe and that a subscriber will not cancel. They call this "Engagement". Users (both paying and non-paying) will be scored from 0.1-1 on a scale. The SambaS engagement score has been shown to correlate with the likelihood of a user subscribing and of a subscriber staying subscribed.

Formula for engagement is below:
```python
# E is engagement = (recency + frequency) / 2
E = (R + F) / 2

# For recency, if you danced today, R = 1. If you danced 4 days ago, R ~= 0.5, and if you danced over 7 days ago, R = 0
R = 1 - (min(7, days_since_last_dance) / 7)

# Frequency is the number of days in which you danced. If you danced 5 times on Monday, and 3 times on Wednesday, but did no other dances in the week, then the frequency is 2 / 7.
F = number_of_days_when_you_danced / 7
```

Given raw user event data [raw_events.csv](/SambaS/raw_events.csv), use the `pandas` library to write a Python script "engagement.py" that reads the data from a csv file, calculates the engagement score for each user for a specific date, and populates a database table with 4 columns - user_id, recency_score, frequency_score, engagement_score, engagement_score_net_change, timestamp.

Your script should append the engagement scores to a table called "engagement" so that the script could be run on a weekly basis, allowing the table to visualise the long-term trend in the engagement score of each user.

**Definition of done**

- There must be a script "engagement.py" that reads data from "raw_events.csv" on successful execution
- Running "engagement.py" must append a new row to the "engagement" table for each user for the current week
- The resulting "engagement" table must have a row per user per week with the following columns
  - user_id - the ID of the user
  - timestamp - the date of the beginning of the week
  - recency_score - the "R" value in the formula above
  - frequency_score - the "F" value in the formula above
  - engagement_score - the "E" value in the formula above
  - engagement_score_net_change - the change in engagement score from the previous week

## Thanks!

If you made it this far, thanks for your time.
We look forward to reviewing your solid application with you!
